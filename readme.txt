Employee application

1. TechStack:
Expressjs
Lokijs
arrayToTree

2. Installation
npm install
npm install --save-dev babel-plugin-module-resolver
npm install --save-dev @babel/plugin-transform-runtime
npm install --save-dev @babel/node
npm install --save-dev @babel/core
npm install --save-dev @babel/core @babel/register

3. Start Development application
cd to nodejs folder tree
npm start
http server will start on port 3001

4. API 
curl commands below or postman collection in doc folder

4.1. localhost:3001/employee/save-employees
save array of employees to db

curl -X POST \
  http://localhost:3001/employee/save-employees \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: bd596f17-930a-4c46-a0be-56cfcb0df1e2' \
  -H 'cache-control: no-cache' \
  -d '{
	"employees": [
		{
			"name": "Alan",
			"id": 100,
			"manager_id": 150
		},
		{
			"name": "Martin",
			"id": 220,
			"manager_id": 100
		},	
		{
			"name": "Jamie",
			"id": 150,
			"manager_id": null
		},	
		{
			"name": "Alex",
			"id": 275,
			"manager_id": 100
		},	
		{
			"name": "Steve",
			"id": 400,
			"manager_id": 150
		},	
		{
			"name": "David",
			"id": 190,
			"manager_id": 400
		}
		
	]
	
}'

4.2. localhost:3001/employee/save-employee
Save single employees

curl -X POST \
  http://localhost:3001/employee/save-employee \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 51' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:3001' \
  -H 'Postman-Token: 7cefca61-c4ea-40e2-9bd1-010f66e93c27,21da6001-d908-482f-b449-52d283396e22' \
  -H 'User-Agent: PostmanRuntime/7.15.2' \
  -H 'cache-control: no-cache' \
  -d '{
	"name": "Tim",
	"id": 710,
	"manager_id": 1500
}'

4.3. localhost:3001/employee/get-employees
List employees in tree format

curl -X POST \
  http://localhost:3001/employee/get-employees \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: ' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:3001' \
  -H 'Postman-Token: 2ce8c1f7-8692-4991-8310-41f6b2b44441,59078c62-6eba-49f0-a516-be1459dd1e7b' \
  -H 'User-Agent: PostmanRuntime/7.15.2' \
  -H 'cache-control: no-cache'

5. Special case
Employee has null manager
Employee has unknown manager id

The application will handle save operation as it is as for me it is a valid case for any organisation.
Employee has null manager, the employee could be another top level manager or manager is unknown when process data entry.
Employee has unknown manager id, manager could be entered after the employee, we can not reject this case.

6. Limitation
No unit test as time constraints
In real life application, consider to change lokijs to graph database
No Validation for input against json schema as time constraints