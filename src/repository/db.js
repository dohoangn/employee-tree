import Loki from "lokijs";
import lfsa from "lokijs/src/loki-fs-structured-adapter";

class Db {
  static initDb() {
    let db = new Loki("employees", {
      autoload: true,
      autosave: true,
      autosaveInterval: 1000,
      throttledSaves: false,
      adapter: new lfsa(),
      autoloadCallback: function() {
        
      },
    });
    return db;
  }
}

export const db = Db.initDb();
