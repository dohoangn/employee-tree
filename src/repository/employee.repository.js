import { db } from './db';

import {Repository} from './base.repository';
class EmployeeRepository extends Repository{
  constructor() {
    super('org', db);
  }
}

export default new EmployeeRepository();
