
export class Repository {
  constructor(collectionName, db) {
    this.collectionName = collectionName;
    this.db = db;
    if (!this.db.getCollection(collectionName)) {
      this.db.addCollection(collectionName);
    }
  }

  clear() {
    let collection = this.db.getCollection(this.collectionName);
    if (collection) {
      collection.chain().remove();
    }
  }

  save(entity) {
    let collection = this.db.getCollection(this.collectionName);
    if (collection) {
      if (!entity.$loki) {
        collection.insert(entity);
      } else {
        collection.update(entity);
      }
      return entity;
    }
  }

  findByLocalId($loki) {
    let collection = this.db.getCollection(this.collectionName);
    if (collection) {
      let result = collection.get($loki);
      return result;
    }
  }

  findById(id) {
    let collection = this.db.getCollection(this.collectionName);
    if (collection) {
      let result = collection.findOne({ id: id });
      return result;
    }
  }

  findOne(criteria) {
    let collection = this.db.getCollection(this.collectionName);
    if (collection) {
      let result = null;
      if (criteria) {
        result = collection.findOne(criteria);
      } else {
        result = collection.findOne();
      }
      return result;
    }
  }

  remove($loki) {
    let collection = this.db.getCollection(this.collectionName);
    if (collection) {
      let result = collection.get($loki);
      if (result) {
        collection.remove(result);
      }
    }
  }

  findAll() {
    let collection = this.db.getCollection(this.collectionName);
    if (collection) {
      let result = collection.find();
      return result;
    }
  }

  find(criteria) {
    let collection = this.db.getCollection(this.collectionName);
    let resultset = null;
    if (collection) {
      if (criteria) {
        resultset = collection.chain().find(criteria);
      } else {
        resultset = collection.chain();
      }      
      return resultset.data();
    }
  }
}
