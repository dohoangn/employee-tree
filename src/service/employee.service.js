import employeeRepository from '../repository/employee.repository';

class EmployeeService {

    saveEmployees(employees) {
        employees.forEach(e => {
            employeeRepository.save(e);
        });
        return employees;
    }

    saveEmployee(employee) {
        return employeeRepository.save(employee)
    }

    getEmployees() {
        return employeeRepository.findAll();
    }
}

export default new EmployeeService();