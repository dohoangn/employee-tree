import express from 'express';

import employeeRoutes from './controller/routes';


//import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
//import session from "express-session";
import morgan from 'morgan';

const port = 3001;
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(morgan('dev'));

app.use('/employee', employeeRoutes);


//end define routes
app.listen(port, () => console.log(`App listening on port ${port}!`));

module.exports = app;
