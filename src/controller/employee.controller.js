
import employeeService from '../service/employee.service';
import arrayToTree from 'array-to-tree';

class EmployeeController {
  async saveEmployees(req, res) {
    let result = employeeService.saveEmployees(req.body.employees);
    return res.json(result);
  }

  async saveEmployee(req, res) {
    let result = employeeService.saveEmployee(req.body);
    return res.json(result);
  }

  async getEmployees(req, res) {
    let result = arrayToTree(employeeService.getEmployees(), {
      parentProperty: 'manager_id',
      customID: 'id'
    });
    return res.json(result);
  }
}

module.exports = new EmployeeController();
