import express from 'express';
import employeeController from './employee.controller';

var router = express.Router();

router.post('/save-employees', employeeController.saveEmployees);
router.post('/get-employees', employeeController.getEmployees);
router.post('/save-employee', employeeController.saveEmployee);

module.exports = router;
